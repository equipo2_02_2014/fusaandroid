Fusa Android
=============

Aplicación diseñada para la Fundación Musical Simón Bolívar para evaluación de la asignatura de Sistemas de Información de la UCLA. Consiste en por este medio, un estudiante de la fundación puede revisar sus clases, sus notas, solicitar el prestamo de algun instrumento y también, ver los eventos próximos, noticias referentes a la fundación entre otras caracteristicas.


Desarrollado por
============

* Juan Labrador - <juanjavierlabrador@gmail.com>

Colaboraciones
===============

* Silvana Dorantes - <silvanadorantes767@gmail.com>
(Implemento la mayoria de los servicios utilizados en Spring.)

* Josif Monroy - <josemonjean@gmail.com>
(Diseño del icono de launcher y otras imagenes usadas.)